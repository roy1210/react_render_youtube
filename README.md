# Render Youtube

Rendering Youtube app

![React_preview](./Scratch.png)

## Dependencies

Install these software to use this app.

- NPM: https://nodejs.org

## Step 1. Clone the project

## Step 2. Install dependencies

```
$ cd React_Render_Youtube

$ npm install
```

## Step 3. Run the Front End Application

`$ npm start`
Visit this URL in your browser: http://localhost:8080
