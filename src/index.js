import _ from "lodash"; //"_" == Lodash
import React, { Component } from "react"; //{Component}を入れることで、`class` + `extend Component`が使用でき、componentsフォルダで作ったクラスをrenderする。
import ReactDOM from "react-dom";
import YTSearch from "youtube-api-search";
import SearchBar from "./components/search_bar";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";

//YouTube Data API v3
const API_key = "AIzaSyAuWWr85-HdbQn9Yh4E_fHUqqOZsKJ0RoM";

// Create a new components. This components should produce some HTML. Below function called "Functional component" JSX render to DOM.
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.videoSearch("Hello world");
  }

  videoSearch(term) {
    YTSearch({ key: API_key, term: term }, videos => {
      this.setState({ videos: videos, selectedVideo: videos[0] });
    });
  }

  render() {
    const videoSearch = _.debounce(term => {
      this.videoSearch(term);
    }, 300);

    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          onVideoSelect={selectedVideo => this.setState({ selectedVideo })}
          videos={this.state.videos}
        />
        {/* videosはVideoListに渡すpropsのイメージ */}
      </div>
    );
  }
}

//Teke this component's generated HTML and put it on the page (in the DOM)
ReactDOM.render(<App />, document.querySelector(".container"));

/*

*1行目、2行目のReactとReact DOMにはfromの後にレファレンスするフォルダを指定する必要がない。node_modulesにあるライブラリを参照だから。自分で作ったcomponentsはフォルダを指定する必要がある。でないと、別々のファイルに同じ名前のクラスやインスタントがあった場合、リアクトはどれを参照すべきかわからなくなる。

*
YTSearch({ key: API_key, term: "surfboards" }, videos => {
      this.setState({ videos });
    });
==>== this.setState({ videos: videos });
  {videos: videos} ES6ではKeyとValueが同じの場合は({videos})と一つだけになるよう省略できる。Valueはアロー関数の引数。

 */
