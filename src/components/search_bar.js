import React, { Component } from "react";

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: "" };
  }

  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)}
        />
      </div>
    );
  }

  onInputChange(term) {
    this.setState({ term });
    this.props.onSearchTermChange(term);
  }
}

export default SearchBar;

/*

* <input /> ==> Should be same as <input></input> jsx format. It's render to DOM.

* Youtube search bar install==> `npm install --save youtube-api-search`

* class SearchBar extends React.Component{ } ==> SearchBarにReact.Componentの機能を拡張させる。継承されるイメージ。In other words it gives our search bar a bunch of functionality from the react doc component class when　we use a class based method.
==> 1行目のimport React, { Component } from "react";とすることで、｀{ Component }`とすることで、 class SearchBar extends Component {}で書き直す事ができる。
{ Component } ==> same as:  const Components = React.component; 

* onChange ==>Event handler あらかじめ定義しておいた関数を、フォーム要素のタグ内から直接呼び出す。

 */
