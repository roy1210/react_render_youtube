import React from "react";

const VideoListItem = ({ video, onVideoSelect }) => {
  //const video = props.video;
  //const onVideoSelect = props.onVideoSelect
  //↑のアロー関数の引数に{ video,onVideoSelect }をしてリファクタリング。
  const imageUrl = video.snippet.thumbnails.default.url;
  const videoTitle = video.snippet.title; //エラー起きたらそのままmedia-headingの{}にvideo.snippet.titleを渡す。

  return (
    <li onClick={() => onVideoSelect(video)} className="list-group-item">
      <div className="video-list media">
        <div className="media-left">
          <img className="media-object" src={imageUrl} />
        </div>
        <div className="media-body">
          <div className="media-heading">{videoTitle}</div>
        </div>
      </div>
    </li>
  );
};

export default VideoListItem;

/*

* const VideoListItem = props => {
  //const video = props.video;
== const VideoListItem = ({video}) => {
videoはvideo_listのVideoListItemに渡す。

*/
