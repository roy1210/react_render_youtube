import React from "react";
import VideoListItem from "./video_list_item";

const VideoList = props => {
  //↑ props ==> index.jsへ渡す　const videos = props.videos; のイメージ
  const videoItems = props.videos.map(video => {
    return (
      <VideoListItem
        onVideoSelect={props.onVideoSelect}
        key={video.etag}
        video={video}
      />
    );
  });
  return (
    <ul className="col-md-4 list-group">
      {videoItems}
      {/* {props.videos.length} */}
    </ul>
  );
};

export default VideoList;

/*
{props.videos.length} ==> classの場合はthis.をつける。
index.js <==> video_listへはfunction base componentからclass base componentsへ渡すときは{props.videos.length}　が　{this.props.videos.length} の意味になる。

*map関数 ==>一つ一つ取り出す。for文のイメージ。
デモ↓

var array = [1, 2, 3];
array.map
array.map(function (number) {return number *2 });
> [2, 4, 6]

array.map(function (number) {return '<div>' + number + '</div>'});

> ["<div>1</div>", "<div>2</div>", "<div>3</div>"]

* etag ==> inspection => Network => command R => search?part=snippet&xxx => item
*/
